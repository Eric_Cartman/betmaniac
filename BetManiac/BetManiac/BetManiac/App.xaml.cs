using BetManiac.Views;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation (XamlCompilationOptions.Compile)]
namespace BetManiac
{
	public partial class App : Application
	{
		public App ()
		{
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MTMyODlAMzEzNjJlMzIyZTMwUkZUME8rYUk1RXpSdmgyRSszWnJKWVlya21FKzQ4UDM4V2JISnN0eGJvMD0=;MTMyOTBAMzEzNjJlMzIyZTMwV01ZUlBqQWZXV0EvUEpiUjhTd0NWc1dNTlRWRWV0aDVlVHdreUhKcldLTT0=;MTMyOTFAMzEzNjJlMzIyZTMwWm5NU3pqUTVqTGtnMWxFRXl2ZUh1d2lJMmhJUnNkUDByVmNSTXZvRnhLdz0=");
            InitializeComponent();

            MainPage = new NavigationPage(new LoginPage());
        }

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
