﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BetManiac.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RegisterPage : ContentPage
	{
		public RegisterPage ()
		{
			InitializeComponent ();
		}

        private void Register_Clicked(object sender, EventArgs e)
        {
            if(emailEntry.Text != "" && usernameEntry.Text != "" && passwordEntry.Text != "" && repeatPasswordEntry.Text != "")
            {
                if(passwordEntry.Text == repeatPasswordEntry.Text)
                {
                    Navigation.PopAsync();
                    DisplayAlert("Almost there", "Check your e-mail to complete the registration!", "OK");
                }
                else
                {
                    DisplayAlert("Error", "Passwords don't match!", "OK");
                }
            }
            else
            {
                DisplayAlert("Alert", "Some mandatory fields are left blank", "OK");
            }
        }
    }
}